import java.util.Scanner;

public class Calculator_Arabic_Roman {

        static int numberOne;
        static int numberTwo;
        static int result;
        static String[] array = new String[3];
        static String[] arrayRomanNumerals = new String[]{"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",};
        static String[] arrayArabicNumerals = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",};

        public static int arithmeticSigns(String[] array, int numberOne, int numberTwo) {

            String sings = array[1];
            int result1 = 101;
            switch (sings) {
                case "+":
                    result1 = numberOne + numberTwo;
                    return result1;
                case "-":
                    result1 = numberOne - numberTwo;
                    return result1;
                case "*":
                    result1 = numberOne * numberTwo;
                    return result1;
                case "/":
                    result1 = numberOne / numberTwo;
                    return result1;
                default:
                    return result1;
            }

        }

        private static int FromRomanToArabic(String roman) {
            int h = -1;
            switch (roman) {
                case "I":
                    h = 1;
                    break;
                case "II":
                    h = 2;
                    break;
                case "III":
                    h = 3;
                    break;
                case "IV":
                    h = 4;
                    break;
                case "V":
                    h = 5;
                    break;
                case "VI":
                    h = 6;
                    break;
                case "VII":
                    h = 7;
                    break;
                case "VIII":
                    h = 8;
                    break;
                case "IX":
                    h = 9;
                    break;
                case "X":
                    h = 10;
                    break;

            }

            return h;
        }

        private static String convertArabicToRoman(int num) {
            String[] roman = {"O", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII",
                    "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI",
                    "XXVII", "XXVIII", "XXIX", "XXX", "XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV", "XXXVI", "XXXVII",
                    "XXXVIII", "XXXIX", "XL", "XLI", "XLII", "XLIII", "XLIV", "XLV", "XLVI", "XLVII", "XLVIII", "XLIX",
                    "L", "LI", "LII", "LIII", "LIV", "LV", "LVI", "LVII", "LVIII", "LIX", "LX",
                    "LXI", "LXII", "LXIII", "LXIV", "LXV", "LXVI", "LXVII", "LXVIII", "LXIX", "LXX",
                    "LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV", "LXXVI", "LXXVII", "LXXVIII", "LXXIX", "LXXX",
                    "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC",
                    "XCI", "XCII", "XCIII", "XCIV", "XCV", "XCVI", "XCVII", "XCVIII", "XCIX", "C"
            };
            return roman[num];
        }

        static boolean compareRomanNumberIndex(String[] arrayRoom, String operand) {
            int n = 0;
            for (int i = 0; i <= arrayRoom.length - 1; i++) {
                if (operand.equals(arrayRoom[i])) {
                    n = 2;
                }
            }
            if (n == 2) {
                return true;
            } else {
                return false;
            }

        }

        static boolean compareArabicNumberIndex(String[] arrayArab, String operand) {
            int n = 0;
            for (int i = 0; i <= arrayArab.length - 1; i++) {
                if (operand.equals(arrayArab[i])) {
                    n = 2;
                }
            }
            if (n == 2) {
                return true;
            } else {
                return false;
            }

        }

        static boolean check_1To_10(int numberOne, int numberTwo) {
            if ((numberOne >= 1 && numberOne <= 10)
                    && (numberTwo >= 1 && numberTwo <= 10)) {

                return true;
            } else {

                return false;
            }
        }


        public static String calc(String input) {
            while (true) {
                array = input.split(" ");
                if (array.length >= 4) {
                    System.out.println("\nOutput: \n");
                    System.out.println("throws Exception //т.к. формат математической операции не удовлетворяет заданию - два операнда и один оператор (+, -, /, *)");
                    break;
                } else if (array.length < 3) {
                    System.out.println("\nOutput: \n");
                    System.out.println("throws Exception //т.к. строка не является математической операцией");
                    break;

                } else {
                    String operand_1 = array[0];
                    String operand_2 = array[2];
                    if (compareRomanNumberIndex(arrayRomanNumerals, operand_1)
                            && compareRomanNumberIndex(arrayRomanNumerals, operand_2)) {
                        numberOne = FromRomanToArabic(operand_1);
                        numberTwo = FromRomanToArabic(operand_2);
                        if (check_1To_10(numberOne, numberTwo)) {
                            result = arithmeticSigns(array, numberOne, numberTwo);
                            if (result <= 1) {
                                System.out.println("\nOutput: \n");
                                System.out.println("throws Exception //т.к. в римской системе нет отрицательных чисел");
                                break;
                            } else if (result == 101) {
                                System.out.println("\nOutput: \n");
                                System.out.println("Введен неверный знак");
                                break;
                            } else {
                                return convertArabicToRoman(result);
                            }
                        } else {
                            System.out.println("\nOutput: \n");
                            System.out.println("Калькулятор должен принимать на вход числа от 1 до 10 включительно, не более. ");
                            break;
                        }


                    } else if ((compareRomanNumberIndex(arrayRomanNumerals, operand_1)
                            && compareArabicNumberIndex(arrayArabicNumerals, operand_2))
                            || (compareRomanNumberIndex(arrayRomanNumerals, operand_2)
                            && compareArabicNumberIndex(arrayArabicNumerals, operand_1))) {
                        System.out.println("\nOutput: \n");
                        System.out.println("throws Exception //т.к. используются одновременно разные системы счисления");
                        break;
                    } else if (compareArabicNumberIndex(arrayArabicNumerals, operand_1)
                            && compareArabicNumberIndex(arrayArabicNumerals, operand_2)) {
                        try {
                            numberOne = Integer.parseInt(array[0]);
                            numberTwo = Integer.parseInt(array[2]);
                        } catch (NumberFormatException e) {
                            System.out.println("\nOutput: \n");
                            System.out.println("Калькулятор умеет работать только с целыми числами.");
                            break;
                        }
                        if (check_1To_10(numberOne, numberTwo)) {
                            result = arithmeticSigns(array, numberOne, numberTwo);
                            if (result == 101) {
                                System.out.println("\nOutput: \n");
                                System.out.println("Введен неверный знак оператора (+, -, /, *)");
                                break;
                            }
                            return Integer.toString(result);

                        } else {
                            System.out.println("\nOutput: \n");
                            System.out.println("Калькулятор должен принимать на вход числа от 1 до 10 включительно, не более. ");
                            break;
                        }

                    } else if (!compareRomanNumberIndex(arrayRomanNumerals, operand_1)
                            || !compareRomanNumberIndex(arrayRomanNumerals, operand_2)) {
                        System.out.println("\nOutput: \n");
                        System.out.println("Калькулятор должен принимать на вход числа от 1 до 10 включительно, не более.");
                        break;
                    }
                }

            }
            return " ";
        }


        public static void main(String[] args) {

            while (true) {

                Scanner scanner = new Scanner(System.in);
                System.out.println("\nInput: \n");
                String input = scanner.nextLine();
                String Output = calc(input);

                if (Output.equals(" ")) {
                    break;
                } else {
                    System.out.println("\nOutput: \n");
                    System.out.println(Output);
                }

            }

        }

}
